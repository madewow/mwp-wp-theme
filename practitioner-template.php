<?php
	/**
	* Template Name: Practitioner Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section id="practitioner-header">
			<header class="header-small" data-interchange="[<?php bloginfo('stylesheet_directory'); ?>/img/header-home.jpg, small]">
				
			</header>
		</section>
		
		<section id="practitioner-info">
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1 text-center">
					<div class="thumb">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
					</div>
					
					<h2 class="text-center">Jane Doe</h2>
					
					<p>Practitioner Type</p>
					
					<ul class="practitioner-action">
						<li>
							<div><i class="fa fa-envelope"></i></div>
							
							<a>Message Practitioner</a>
						</li>
						
						<li>
							<div><i class="fa fa-bookmark"></i></div>
							
							<a>Bookmark</a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non massa et mi suscipit dapibus. Duis eget nisl id ipsum consequat aliquam a ac felis. Proin non neque nibh. Suspendisse sodales arcu id dui suscipit fringilla. Aliquam quis purus et justo lacinia porttitor non vel risus. Fusce pharetra hendrerit sapien et finibus. Ut facilisis, purus nec convallis tincidunt, ligula orci dictum ipsum, id consectetur sem lectus vel nunc.</p>
					
					<p>Praesent dictum purus a tellus feugiat, non blandit neque vestibulum. Donec et orci vel velit sodales hendrerit nec at arcu. Sed pellentesque augue vitae diam lobortis, vel porttitor nisl auctor. Aenean id malesuada nulla, at luctus dui. Integer at nisl enim. Vivamus nec erat sit amet magna laoreet tincidunt nec pharetra tortor.</p>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-6">
							<h3>Address</h3>
							
							<p>123 Main Street<br>Vancouver, British Columbia<br>CANADA V6C 2Y9</p>
						</div>
						
						<div class="columns small-12 medium-6">
							<h3>Business Hours</h3>
							
							<div class="row">
								<div class="columns small-12 medium-6">
									<p>Mon: 10 am - 10 pm<br>Tue: 10 am - 10 pm<br>Wed: 10 am - 10 pm<br>Thu: 10 am - 10 pm</p>
								</div>
								
								<div class="columns small-12 medium-6">
									<p>Fri: 10 am - 10 pm<br>Sat: closed<br>Sun: closed</p>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section id="practioner-map">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Location</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="map" id="map">
				
			</div>
			
			<p class="text-center">Practitioner is <strong><span id="distance"></span></strong> away from your location</p>
		</section>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		the_content();
		endwhile; else: ?>
		<p>Sorry, no posts matched your criteria.</p>
		<?php endif; ?>
		
		<?php $author_id = 1; /* get the logged-in user id */ include( locate_template( 'parts/recommended-posts.php', false, false ) );  ?>
		
		<?php $author_id = 1; /* get the logged-in user id */ include( locate_template( 'parts/testimonials.php', false, false ) );  ?>


<?php get_footer(); ?>