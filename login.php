<?php
	/**
	* Template Name: Login Page Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>
		<section id="login">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Practitioner Login</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-4 medium-offset-4">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
				the_content();
				endwhile; else: ?>
				<p>Sorry, no posts matched your criteria.</p>
				<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<form id="login-form" class="columns small-12 small-offset-0 medium-4 medium-offset-4">
					<div class="row">
						<div class="columns small-12">
							<input type="text" placeholder="Username">
						</div>
						
						<div class="columns small-12">
							<input type="password" placeholder="Password">
						</div>
						
						<div class="columns small-12">
							<button class="button purple">Login</button>
							
							<p><a><small>Forgot password?</small></a>&nbsp;&nbsp;<a><small>Create Account</small></a></p>
						</div>
					</div>
				</form>
			</div>
		</section>

<?php get_footer(); ?>