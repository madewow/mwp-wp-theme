<?php
	/**
	* Template Name: Register PMPRO Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>
		<section>
			<header class="header-small" data-interchange="[img/header-home.jpg, small]">
				
			</header>
		</section>
		
		<section>
			<div class="row">
				<div class="columns small-12">
					<form id="pmpro_form" class="pmpro_form" action="" method="post">
						<table id="pmpro_pricing_fields" class="pmpro_checkout" width="100%" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th>
										<span class="pmpro_thead-name">Membership Level</span>
										<span class="pmpro_thead-msg"><a href="http://madejkt.com/membership-account/membership-levels/">change</a></span>
									</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
										<p>You have selected the <strong>Platinum</strong> membership level.</p>
						
										<p>Text, image &amp; video option<br>Customizable image background<br>
						Membership/access to biz courses<br>
						How to’s<br>
						Link/integrate with appt booking<br>
						Blogging capability (with admin edit/approval by us)<br>
						Reviews/star rating<br>
						Website (with tutorials)<br>
						Migration from other wordpress sites</p>
						
										<div id="pmpro_level_cost">
											<p>The price for membership is <strong>$49.99 per Month</strong>. </p>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						
						<table id="pmpro_user_fields" class="pmpro_checkout" width="100%" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th>
										<span class="pmpro_thead-name">Account Information</span>
										<span class="pmpro_thead-msg">Already have an account? <a href="http://madejkt.com/mwp-login?redirect_to=http%3A%2F%2Fmadejkt.com%2Fmembership-account%2Fmembership-checkout%2F%3Flevel%3D3">Log in here</a></span>
									</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
										<div>
											<label for="username">Username</label>
											<input id="username" name="username" type="text" class="input pmpro_required" size="30" value=""><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
										</div>
						
										
										<div>
											<label for="password">Password</label>
											<input id="password" name="password" type="password" class="input pmpro_required" size="30" value=""><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
										</div>
										
										<div>
												<label for="password2">Confirm Password</label>
												<input id="password2" name="password2" type="password" class="input pmpro_required" size="30" value=""><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
										</div>
											
										
										<div>
											<label for="bemail">E-mail Address</label>
											<input id="bemail" name="bemail" type="email" class="input pmpro_required" size="30" value=""><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
										</div>
										
										<div>
												<label for="bconfirmemail">Confirm E-mail Address</label>
												<input id="bconfirmemail" name="bconfirmemail" type="email" class="input pmpro_required" size="30" value="">
												<span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
						
										</div>
											
										
										<div class="pmpro_hidden">
											<label for="fullname">Full Name</label>
											<input id="fullname" name="fullname" type="text" class="input " size="30" value=""> <strong>LEAVE THIS BLANK</strong>
										</div>
						
										<div class="pmpro_captcha">
														</div>
						
										
									</td>
								</tr>
							</tbody>
						</table>
						
						<table id="pmpro_site_fields" class="pmpro_checkout top1em" width="100%" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th>
										<span class="pmpro_thead-name">Site Information</span>
									</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
									
													
										<div>
											<label for="sitename">Site Name</label>
											<input id="sitename" name="sitename" type="text" class="input" size="30" value="">
											<span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>			
											<div><strong>Your address will be {site name}.madejkt.com/</strong><br>Must be at least 4 characters, letters and numbers only. It cannot be changed, so choose carefully!</div>
										</div>
										
										<div>
											<label for="sitetitle">Site Title</label>
											<input id="sitetitle" name="sitetitle" type="text" class="input" size="30" value="">
										</div> 
													
									
									</td>
								</tr>
							</tbody>
						</table>
						
						<table id="pmpro_billing_address_fields" class="pmpro_checkout top1em" width="100%" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th>
										<span class="pmpro_thead-name">Billing Address</span>
									</th>
								</tr>
							</thead>
							
							<tbody>
								<tr>
									<td>
										<div>
											<label for="bfirstname">First Name</label>
											<input id="bfirstname" name="bfirstname" type="text" class="input " size="30" value="">
										</div>
										<div>
											<label for="blastname">Last Name</label>
											<input id="blastname" name="blastname" type="text" class="input " size="30" value="">
										</div>
										<div>
											<label for="baddress1">Address 1</label>
											<input id="baddress1" name="baddress1" type="text" class="input " size="30" value="">
										</div>
										<div>
											<label for="baddress2">Address 2</label>
											<input id="baddress2" name="baddress2" type="text" class="input " size="30" value="">
										</div>
						
															<div>
												<label for="bcity">City</label>
												<input id="bcity" name="bcity" type="text" class="input " size="30" value="">
											</div>
											<div>
												<label for="bstate">State</label>
												<input id="bstate" name="bstate" type="text" class="input " size="30" value="">
											</div>
											<div>
												<label for="bzipcode">Postal Code</label>
												<input id="bzipcode" name="bzipcode" type="text" class="input " size="30" value="">
											</div>
										
														<div>
											<label for="bcountry">Country</label>
											<select name="bcountry" class=" ">
																			<option value="AF">Afghanistan</option>
																				<option value="AX">Aland Islands</option>
																				<option value="AL">Albania</option>
																				<option value="DZ">Algeria</option>
																				<option value="AS">American Samoa</option>
																				<option value="AD">Andorra</option>
																				<option value="AO">Angola</option>
																				<option value="AI">Anguilla</option>
																				<option value="AQ">Antarctica</option>
																				<option value="AG">Antigua and Barbuda</option>
																				<option value="AR">Argentina</option>
																				<option value="AM">Armenia</option>
																				<option value="AW">Aruba</option>
																				<option value="AU">Australia</option>
																				<option value="AT">Austria</option>
																				<option value="AZ">Azerbaijan</option>
																				<option value="BS">Bahamas</option>
																				<option value="BH">Bahrain</option>
																				<option value="BD">Bangladesh</option>
																				<option value="BB">Barbados</option>
																				<option value="BY">Belarus</option>
																				<option value="BE">Belgium</option>
																				<option value="BZ">Belize</option>
																				<option value="BJ">Benin</option>
																				<option value="BM">Bermuda</option>
																				<option value="BT">Bhutan</option>
																				<option value="BO">Bolivia</option>
																				<option value="BA">Bosnia and Herzegovina</option>
																				<option value="BW">Botswana</option>
																				<option value="BV">Bouvet Island</option>
																				<option value="BR">Brazil</option>
																				<option value="IO">British Indian Ocean Territory</option>
																				<option value="VG">British Virgin Islands</option>
																				<option value="BN">Brunei</option>
																				<option value="BG">Bulgaria</option>
																				<option value="BF">Burkina Faso</option>
																				<option value="BI">Burundi</option>
																				<option value="KH">Cambodia</option>
																				<option value="CM">Cameroon</option>
																				<option value="CA">Canada</option>
																				<option value="CV">Cape Verde</option>
																				<option value="KY">Cayman Islands</option>
																				<option value="CF">Central African Republic</option>
																				<option value="TD">Chad</option>
																				<option value="CL">Chile</option>
																				<option value="CN">China</option>
																				<option value="CX">Christmas Island</option>
																				<option value="CC">Cocos (Keeling) Islands</option>
																				<option value="CO">Colombia</option>
																				<option value="KM">Comoros</option>
																				<option value="CG">Congo (Brazzaville)</option>
																				<option value="CD">Congo (Kinshasa)</option>
																				<option value="CK">Cook Islands</option>
																				<option value="CR">Costa Rica</option>
																				<option value="HR">Croatia</option>
																				<option value="CU">Cuba</option>
																				<option value="CY">Cyprus</option>
																				<option value="CZ">Czech Republic</option>
																				<option value="DK">Denmark</option>
																				<option value="DJ">Djibouti</option>
																				<option value="DM">Dominica</option>
																				<option value="DO">Dominican Republic</option>
																				<option value="EC">Ecuador</option>
																				<option value="EG">Egypt</option>
																				<option value="SV">El Salvador</option>
																				<option value="GQ">Equatorial Guinea</option>
																				<option value="ER">Eritrea</option>
																				<option value="EE">Estonia</option>
																				<option value="ET">Ethiopia</option>
																				<option value="FK">Falkland Islands</option>
																				<option value="FO">Faroe Islands</option>
																				<option value="FJ">Fiji</option>
																				<option value="FI">Finland</option>
																				<option value="FR">France</option>
																				<option value="GF">French Guiana</option>
																				<option value="PF">French Polynesia</option>
																				<option value="TF">French Southern Territories</option>
																				<option value="GA">Gabon</option>
																				<option value="GM">Gambia</option>
																				<option value="GE">Georgia</option>
																				<option value="DE">Germany</option>
																				<option value="GH">Ghana</option>
																				<option value="GI">Gibraltar</option>
																				<option value="GR">Greece</option>
																				<option value="GL">Greenland</option>
																				<option value="GD">Grenada</option>
																				<option value="GP">Guadeloupe</option>
																				<option value="GU">Guam</option>
																				<option value="GT">Guatemala</option>
																				<option value="GG">Guernsey</option>
																				<option value="GN">Guinea</option>
																				<option value="GW">Guinea-Bissau</option>
																				<option value="GY">Guyana</option>
																				<option value="HT">Haiti</option>
																				<option value="HM">Heard Island and McDonald Islands</option>
																				<option value="HN">Honduras</option>
																				<option value="HK">Hong Kong S.A.R., China</option>
																				<option value="HU">Hungary</option>
																				<option value="IS">Iceland</option>
																				<option value="IN">India</option>
																				<option value="ID">Indonesia</option>
																				<option value="IR">Iran</option>
																				<option value="IQ">Iraq</option>
																				<option value="IE">Ireland</option>
																				<option value="IM">Isle of Man</option>
																				<option value="IL">Israel</option>
																				<option value="IT">Italy</option>
																				<option value="CI">Ivory Coast</option>
																				<option value="JM">Jamaica</option>
																				<option value="JP">Japan</option>
																				<option value="JE">Jersey</option>
																				<option value="JO">Jordan</option>
																				<option value="KZ">Kazakhstan</option>
																				<option value="KE">Kenya</option>
																				<option value="KI">Kiribati</option>
																				<option value="KW">Kuwait</option>
																				<option value="KG">Kyrgyzstan</option>
																				<option value="LA">Laos</option>
																				<option value="LV">Latvia</option>
																				<option value="LB">Lebanon</option>
																				<option value="LS">Lesotho</option>
																				<option value="LR">Liberia</option>
																				<option value="LY">Libya</option>
																				<option value="LI">Liechtenstein</option>
																				<option value="LT">Lithuania</option>
																				<option value="LU">Luxembourg</option>
																				<option value="MO">Macao S.A.R., China</option>
																				<option value="MK">Macedonia</option>
																				<option value="MG">Madagascar</option>
																				<option value="MW">Malawi</option>
																				<option value="MY">Malaysia</option>
																				<option value="MV">Maldives</option>
																				<option value="ML">Mali</option>
																				<option value="MT">Malta</option>
																				<option value="MH">Marshall Islands</option>
																				<option value="MQ">Martinique</option>
																				<option value="MR">Mauritania</option>
																				<option value="MU">Mauritius</option>
																				<option value="YT">Mayotte</option>
																				<option value="MX">Mexico</option>
																				<option value="FM">Micronesia</option>
																				<option value="MD">Moldova</option>
																				<option value="MC">Monaco</option>
																				<option value="MN">Mongolia</option>
																				<option value="ME">Montenegro</option>
																				<option value="MS">Montserrat</option>
																				<option value="MA">Morocco</option>
																				<option value="MZ">Mozambique</option>
																				<option value="MM">Myanmar</option>
																				<option value="NA">Namibia</option>
																				<option value="NR">Nauru</option>
																				<option value="NP">Nepal</option>
																				<option value="NL">Netherlands</option>
																				<option value="AN">Netherlands Antilles</option>
																				<option value="NC">New Caledonia</option>
																				<option value="NZ">New Zealand</option>
																				<option value="NI">Nicaragua</option>
																				<option value="NE">Niger</option>
																				<option value="NG">Nigeria</option>
																				<option value="NU">Niue</option>
																				<option value="NF">Norfolk Island</option>
																				<option value="KP">North Korea</option>
																				<option value="MP">Northern Mariana Islands</option>
																				<option value="NO">Norway</option>
																				<option value="OM">Oman</option>
																				<option value="PK">Pakistan</option>
																				<option value="PW">Palau</option>
																				<option value="PS">Palestinian Territory</option>
																				<option value="PA">Panama</option>
																				<option value="PG">Papua New Guinea</option>
																				<option value="PY">Paraguay</option>
																				<option value="PE">Peru</option>
																				<option value="PH">Philippines</option>
																				<option value="PN">Pitcairn</option>
																				<option value="PL">Poland</option>
																				<option value="PT">Portugal</option>
																				<option value="PR">Puerto Rico</option>
																				<option value="QA">Qatar</option>
																				<option value="RE">Reunion</option>
																				<option value="RO">Romania</option>
																				<option value="RU">Russia</option>
																				<option value="RW">Rwanda</option>
																				<option value="BL">Saint Barthelemy</option>
																				<option value="SH">Saint Helena</option>
																				<option value="KN">Saint Kitts and Nevis</option>
																				<option value="LC">Saint Lucia</option>
																				<option value="MF">Saint Martin (French part)</option>
																				<option value="PM">Saint Pierre and Miquelon</option>
																				<option value="VC">Saint Vincent and the Grenadines</option>
																				<option value="WS">Samoa</option>
																				<option value="SM">San Marino</option>
																				<option value="ST">Sao Tome and Principe</option>
																				<option value="SA">Saudi Arabia</option>
																				<option value="SN">Senegal</option>
																				<option value="RS">Serbia</option>
																				<option value="SC">Seychelles</option>
																				<option value="SL">Sierra Leone</option>
																				<option value="SG">Singapore</option>
																				<option value="SK">Slovakia</option>
																				<option value="SI">Slovenia</option>
																				<option value="SB">Solomon Islands</option>
																				<option value="SO">Somalia</option>
																				<option value="ZA">South Africa</option>
																				<option value="GS">South Georgia and the South Sandwich Islands</option>
																				<option value="KR">South Korea</option>
																				<option value="ES">Spain</option>
																				<option value="LK">Sri Lanka</option>
																				<option value="SD">Sudan</option>
																				<option value="SR">Suriname</option>
																				<option value="SJ">Svalbard and Jan Mayen</option>
																				<option value="SZ">Swaziland</option>
																				<option value="SE">Sweden</option>
																				<option value="CH">Switzerland</option>
																				<option value="SY">Syria</option>
																				<option value="TW">Taiwan</option>
																				<option value="TJ">Tajikistan</option>
																				<option value="TZ">Tanzania</option>
																				<option value="TH">Thailand</option>
																				<option value="TL">Timor-Leste</option>
																				<option value="TG">Togo</option>
																				<option value="TK">Tokelau</option>
																				<option value="TO">Tonga</option>
																				<option value="TT">Trinidad and Tobago</option>
																				<option value="TN">Tunisia</option>
																				<option value="TR">Turkey</option>
																				<option value="TM">Turkmenistan</option>
																				<option value="TC">Turks and Caicos Islands</option>
																				<option value="TV">Tuvalu</option>
																				<option value="VI">U.S. Virgin Islands</option>
																				<option value="USAF">US Armed Forces</option>
																				<option value="UG">Uganda</option>
																				<option value="UA">Ukraine</option>
																				<option value="AE">United Arab Emirates</option>
																				<option value="GB">United Kingdom</option>
																				<option value="US" selected="selected">United States</option>
																				<option value="UM">United States Minor Outlying Islands</option>
																				<option value="UY">Uruguay</option>
																				<option value="UZ">Uzbekistan</option>
																				<option value="VU">Vanuatu</option>
																				<option value="VA">Vatican</option>
																				<option value="VE">Venezuela</option>
																				<option value="VN">Vietnam</option>
																				<option value="WF">Wallis and Futuna</option>
																				<option value="EH">Western Sahara</option>
																				<option value="YE">Yemen</option>
																				<option value="ZM">Zambia</option>
																				<option value="ZW">Zimbabwe</option>
																		</select>
										</div>
														<div>
											<label for="bphone">Phone</label>
											<input id="bphone" name="bphone" type="text" class="input " size="30" value="">
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						
						<table id="pmpro_payment_information_fields" class="pmpro_checkout top1em" width="100%" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th>
										<span class="pmpro_thead-name">Payment Information</span>
										<span class="pmpro_thead-msg">We Accept Visa, Mastercard, American Express and Discover</span>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr valign="top">
									<td>
										
																<input type="hidden" id="CardType" name="CardType" value="Unknown Card Type">
											<script>
												<!--
												jQuery(document).ready(function() {
														jQuery('#AccountNumber').validateCreditCard(function(result) {
															var cardtypenames = {
																"amex"                      : "American Express",
																"diners_club_carte_blanche" : "Diners Club Carte Blanche",
																"diners_club_international" : "Diners Club International",
																"discover"                  : "Discover",
																"jcb"                       : "JCB",
																"laser"                     : "Laser",
																"maestro"                   : "Maestro",
																"mastercard"                : "Mastercard",
																"visa"                      : "Visa",
																"visa_electron"             : "Visa Electron"
															};
					
															if(result.card_type)
																jQuery('#CardType').val(cardtypenames[result.card_type.name]);
															else
																jQuery('#CardType').val('Unknown Card Type');
														});
												});
												-->
											</script>
											
										<div class="pmpro_payment-account-number">
											<label for="AccountNumber">Card Number</label>
											<input id="AccountNumber" name="AccountNumber" class="input pmpro_required" type="text" size="25" value="" data-encrypted-name="number" autocomplete="off"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
										</div>
					
										<div class="pmpro_payment-expiration">
											<label for="ExpirationMonth">Expiration Date</label>
											<select id="ExpirationMonth" name="ExpirationMonth" class=" pmpro_required">
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
												<option value="06">06</option>
												<option value="07">07</option>
												<option value="08">08</option>
												<option value="09">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											</select><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>/<select id="ExpirationYear" name="ExpirationYear" class=" pmpro_required">
																				<option value="2017">2017</option>
																				<option value="2018">2018</option>
																				<option value="2019">2019</option>
																				<option value="2020">2020</option>
																				<option value="2021">2021</option>
																				<option value="2022">2022</option>
																				<option value="2023">2023</option>
																				<option value="2024">2024</option>
																				<option value="2025">2025</option>
																				<option value="2026">2026</option>
																		</select><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
										</div>
					
															<div class="pmpro_payment-cvv">
											<label for="CVV">CVV</label>
											<input class="input" id="CVV" name="CVV" type="text" size="4" value="">  <small>(<a href="javascript:void(0);" onclick="javascript:window.open('http://madejkt.com/wp-content/plugins/paid-memberships-pro/pages/popup-cvv.html','cvv','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=600, height=475');">what's this?</a>)</small>
										</div>
										
										
									</td>
								</tr>
							</tbody>
						</table>
						
						<table id="pmpro_tos_fields" class="pmpro_checkout top1em" width="100%" cellpadding="0" cellspacing="0" border="0">
							<thead>
								<tr>
									<th><span class="pmpro_thead-name">Membership Billing</span></th>
								</tr>
							</thead>
							
							<tbody>
								<tr class="odd">
									<td>
										<div id="pmpro_license">
					<p>Membership billing terms and condition here</p>
										</div>
										<input type="checkbox" name="tos" value="1" id="tos"> <label class="pmpro_normal pmpro_clickable" for="tos">I agree to the Membership Billing</label>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</section>
		
<?php get_footer(); ?>