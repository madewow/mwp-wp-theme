<?php get_header(); ?>
		
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<section id="article-header">
			<header class="header-small" data-interchange="[<?php echo get_the_post_thumbnail_url($post->ID, "full"); ?>, small]">
				<div class="container">
					<div class="row">
						<div class="columns small-12 small-offset-0 large-8 large-offset-2">
							<h1 class="text-center"><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</header>
		</section>
		
		<section id="practitioner-info">
			<div class="row">
				<div class="small-12 small-offseet-0 medium-10 medium-offset-1 text-center">
					<div class="thumb">
						<img src="img/tn_profile.jpg">
					</div>
					
					<h2 class="text-center"><?php the_author(); ?></h2>
					</ul>
				</div>
			</div>
		</section>
		
		<?php get_template_part( 'parts/loop', 'single' ); ?>
		
		<?php get_template_part( 'parts/recommended', 'posts' ); ?>
		
		<?php endwhile; else : ?>
					 
		<?php endif; ?>
					
<?php get_footer(); ?>