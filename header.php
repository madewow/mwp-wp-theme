<?php
?><!doctype html>
<html class="no-js" lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	
	<body>
		<nav id="main-navigation">
			<div class="row">
				<div class="columns small-12 medium-6">
					<a class="logo-text"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-text.png"></a>
				</div>
				
				<div class="columns small-12 medium-6">
					<?php if (get_page_template_slug( $post_id ) == "practitioner-dashboard-template.php") { ?> 
						<ul id="menu-main-navigation">
							<li>
								<a>Main Site</a>
							</li>
							
							<li class="has-sub">
								<a>Hi, Jane Doe</a>
								
								<a class="arrow"><i class="fa fa-chevron-down"></i></a>
								
								<ul>
									<li><a>Manage Personal Information</a></li>
									<li><a>Manage Practitioner Information</a></li>
									<li><a>Manage Blog</a></li>
									<li><a>Manage Testimonials</a></li>
									<li>
										<a>Messages</a>
										<span>1</span>	
									</li>
									<li><a>Logout</a></li>
								</ul>
							</li>
						</ul>
					
					<?php } else { ?>
						<?php joints_top_nav(); ?>
					<?php } ?>
				</div>
			</div>
			
			<a class="logo-mark"></a>
		</nav>