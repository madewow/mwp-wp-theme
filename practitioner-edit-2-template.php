<?php
	/**
	* Template Name: Practitioner Edit-2 Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section id="practitioner-header">
			<header class="header-small" data-interchange="[<?php bloginfo('stylesheet_directory'); ?>/img/header-home.jpg, small]">
				<div class="row">
					<div class="columns small-12 small-offset-0 medium-10 medium-offset-1 text-center">
						<a class="camera" data-open="upload-background-modal">
							<i class="fa fa-camera"></i>
						</a>
					</div>
				</div>
			</header>
		</section>
		
		<section id="practitioner-info">
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1 text-center">
					<div class="thumb">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
						
						<a class="camera" data-open="upload-background-modal">
							<i class="fa fa-camera"></i>
						</a>
					</div>
					
					<h2 class="text-center">Jane Doe</h2>
					
					<p>Practitioner Type</p>
				</div>
			</div>
		</section>
		
		<form id="register-form">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Location</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12">
							<label>Address</label>
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<input type="text" placeholder="Address Line 1">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<input type="text" placeholder="Address Line 2">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-6">
							<label>City</label>
							
							<input type="text">
						</div>
						
						<div class="columns small-12 medium-6">
							<label>State / Province / Region</label>
							
							<input type="text">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-6">
							<label>ZIP / Postal Code</label>
							
							<input type="text">
						</div>
						
						<div class="columns small-12 medium-6">
							<label>Country</label>
							
							<select>
								<option>Country</option>
							</select>
						</div>
					</div>
					
					<div class="row" id="practioner-map">
						<div class="columns small-12">
							<!-- map goes here -->
							<div class="map" id="map"></div>
						</div>
					</div>
				</div>
			</div>
			
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Business Hours</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-3">
							<p>Monday</p>
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Open">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Close">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Notes">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-3">
							<p>Tuesday</p>
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Open">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Close">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Notes">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-3">
							<p>Wednesday</p>
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Open">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Close">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Notes">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-3">
							<p>Thursday</p>
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Open">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Close">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Notes">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-3">
							<p>Friday</p>
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Open">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Close">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Notes">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-3">
							<p>Saturday</p>
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Open">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Close">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Notes">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-3">
							<p>Sunday</p>
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Open">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Close">
						</div>
						
						<div class="columns small-12 medium-3">
							<input type="text" placeholder="Notes">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<p><em>Leave empty on closed days</em></p>
						</div>
					</div>
				</div>
			</div>
			
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Social Media</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row" id="social-media-fields">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-6">
							<input type="text" placeholder="Facebook Company Page">
							
							<div class="icon-socmed"><i class="fa fa-facebook"></i></div>
						</div>
						
						<div class="columns small-12 medium-6">
							<input type="text" placeholder="Twitter">
							
							<div class="icon-socmed"><i class="fa fa-twitter"></i></div>
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-6">
							<input type="text" placeholder="LinkedIn">
							
							<div class="icon-socmed"><i class="fa fa-linkedin"></i></div>
						</div>
						
						<div class="columns small-12 medium-6">
							<input type="text" placeholder="Google Plus">
							
							<div class="icon-socmed"><i class="fa fa-google-plus"></i></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1 text-right">
					<button class="button">SUBMIT</button>
				</div>
			</div>
		</form>
		
		<div class="reveal" id="upload-background-modal" data-reveal>
			<div class="row">
				<div class="columns small-12 text-center">
					<p>Upload Background Image</p>
					
					<h4>Drop file here to upload</h4>
					
					<p>or</p>
					
					<div class="container">
						<button class="button">Choose File</button>
						
						<input type="file">
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>