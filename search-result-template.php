<?php
	/**
	* Template Name: Search Result Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section>
			<div class="row">
				<div class="columns small-12 small-offset-0 large-8 large-offset-2">
					<form class="form-search">
						<div class="row">
							<div class="columns small-12">
								<label>Search results near</label>
								
								<div style="position: relative">
									<input type="text" id="location-field" class="field-location" placeholder="Your location, address, region, city, postal code" value="South Jakarta, South Jakarta City, Jakarta, Indonesia">
									
									<a class="btn-location"><i class="fa fa-map-o"></i></a>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="columns small-12">
								<label>Practitioner types:</label>
								
								<select id="practitioner-type" multiple="mulitple">
									<option selected="selected">practitioner-type-1</option>
									<option>practitioner-type-2</option>
									<option>practitioner-type-3</option>
									<option selected="selected">practitioner-type-4</option>
									<option>practitioner-type-5</option>
									<option selected="selected">practitioner-type-6</option>
									<option>practitioner-type-7</option>
									<option>practitioner-type-8</option>
									<option>practitioner-type-9</option>
									<option>practitioner-type-10</option>
								</select>
							</div>
						</div>
						
						<br>
						
						<div class="row">
							<div class="columns small-12">
								<ul class="accordion condition-filter" data-accordion data-allow-all-closed="true">
									<li class="accordion-item" data-accordion-item>
								    	<a href="#" class="accordion-title">Refine search result (by practitioner conditions):</a>
								    
									    <div class="accordion-content" data-tab-content>
										    <div class="ginput_container ginput_container_checkbox">
										    	<ul class="gfield_checkbox" id="input_1_21">
											    	<li class="gchoice_1_21_1" style="display: list-item;">
														<input name="input_21.1" type="checkbox" value="Orthotics" id="choice_1_21_1" tabindex="11">
														<label for="choice_1_21_1" id="label_1_21_1">Orthotics</label>
													</li>
													
													<li class="gchoice_1_21_2" style="display: list-item;">
														<input name="input_21.2" type="checkbox" value="IMS" id="choice_1_21_2" tabindex="12">
														<label for="choice_1_21_2" id="label_1_21_2">IMS</label>
													</li>
													
													<li class="gchoice_1_21_3">
														<input name="input_21.3" type="checkbox" value="Home Birth" id="choice_1_21_3" tabindex="13">
														<label for="choice_1_21_3" id="label_1_21_3">Home Birth</label>
													</li>
										    	</ul>
										    </div>
									    </div>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="row">
							<div class="columns small-12 text-center">
								<button class="button purple">Search Again</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
		
		<section class="search-result-container">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Within 10 km</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="small-12 small-offseet-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul class="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul class="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="small-12 small-offseet-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul class="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul class="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="small-12 text-center">
					<a class="button">More Results</a>
				</div>
			</div>
		</section>

<?php get_footer(); ?>