<?php
	/**
	* Template Name: Practitioner Dashboard Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section id="practitioner-header">
			<header class="header-small" data-interchange="[<?php bloginfo('stylesheet_directory'); ?>/img/header-home.jpg, small]">
				<div class="row">
					<div class="small-12 small-offseet-0 medium-10 medium-offset-1 text-center">
						<a class="camera" data-open="upload-background-modal">
							<i class="fa fa-camera"></i>
						</a>
					</div>
				</div>
			</header>
		</section>
		
		<section id="practitioner-info">
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1 text-center">
					<div class="thumb">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
						
						<a class="camera" data-open="upload-background-modal">
							<i class="fa fa-camera"></i>
						</a>
					</div>
					
					<h2 class="text-center">Jane Doe</h2>
					
					<p>Practitioner Type</p>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<div class="dashboard row small-up-2 medium-up-4 text-center">
						<div class="columns column-block">
							<a class="icon">
								<i class="fa fa-lg fa-user"></i>
							</a>
							
							<p>Edit Personal Information</p>
						</div>
						
						<div class="columns column-block">
							<a class="icon">
								<i class="fa fa-lg fa-heartbeat"></i>
							</a>
							
							<p>Edit Practitioner Information</p>
						</div>
						
						<div class="columns column-block">
							<a class="icon">
								<i class="fa fa-lg fa-edit"></i>
							</a>
							
							<p>Manage Blog</p>
						</div>
						
						<div class="columns column-block">
							<a class="icon">
								<i class="fa fa-lg fa-comment"></i>
							</a>
							
							<p>Manage Testimonial</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<?php $author_id = 1; /* get the logged-in user id */ include( locate_template( 'parts/recommended-posts.php', false, false ) );  ?>
		
		<?php $author_id = 1; /* get the logged-in user id */ include( locate_template( 'parts/testimonials.php', false, false ) );  ?>

<?php get_footer(); ?>