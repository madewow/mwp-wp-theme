		<article>
			<div class="row">
				<div class="small-12 small-offseet-0 medium-8 medium-offset-2">
					<?php the_content(); ?>
					
					<hr class="final">
				</div>
			</div>
		</article>