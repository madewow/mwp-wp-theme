		<section id="home-recommended-reading">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Testimonials</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="testimonials">
				<div class="row">
					<div class="columns small-12 small-offseet-0 medium-8 medium-offset-2">
						<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam lobortis convallis dolor eu sodales. Maecenas auctor, lacus ac posuere condimentum, tortor arcu placerat nunc, quis molestie leo lacus vel neque. Sed sed sagittis ante. Vivamus euismod nisl eu luctus dignissim. </blockquote>
						
						<cite>John Doe<br>CEO ACME Inc.</cite>
						
						<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam lobortis convallis dolor eu sodales. Maecenas auctor, lacus ac posuere condimentum, tortor arcu placerat nunc, quis molestie leo lacus vel neque. Sed sed sagittis ante. Vivamus euismod nisl eu luctus dignissim. </blockquote>
						
						<cite>John Doe<br>CEO ACME Inc.</cite>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12  large-8 large-offset-2">
					<p><a class="button-edit"><i class="fa fa-pencil"></i>Manage Testimonials</a></p>
				</div>
			</div>
		</section>