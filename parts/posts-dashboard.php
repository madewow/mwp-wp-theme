		<section id="home-recommended-reading">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Blog</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="recommended-article">
			<?php
				$args	= array(
						    'author'        =>  $author_id,
						    'orderby'       =>  'post_date',
						    'order'         =>  'ASC' 
						    );
						    
				$posts = get_posts( $args );
			?>
			
			<?php $i = 0; foreach($posts as $post) { ?>
				<div class="row">
				<?php if (0 == $i % 2) { ?> 
					<div class="columns small-12 large-7 large-offset-1">
						<h3><?php echo $post->post_title; ?></h3>
						
						<p><?php echo $post->post_excerpt; ?></p>
					</div>
			
				
					<div class="columns small-12 large-3 end hide-for-small-only">
						<div class="thumb">
							<?php echo get_the_post_thumbnail($post->ID, "medium"); ?>
							
							<a>
								<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
							</a>	
						</div>
					</div>
					
				<?php } else { ?>
					<div class="columns small-12 large-3 large-offset-1 hide-for-small-only">
						<div class="thumb">
							<?php echo get_the_post_thumbnail($post->ID, "medium"); ?>
							
							<a>
								<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
							</a>	
						</div>
					</div>
					
					<div class="columns small-12 large-7 end">
						<h3><?php echo $post->post_title; ?></h3>
						
						<p><?php echo $post->post_excerpt; ?></p>
					</div>
				<?php } ?>
				</div>
			<?php $i++; } ?>
			</div>
			
			<div class="row">
				<div class="columns small-12  large-8 large-offset-1">
					<p><a class="button-edit"><i class="fa fa-pencil"></i>Manage Blog</a></p>
				</div>
			</div>
		</section>