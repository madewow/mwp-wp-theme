<?php
	/**
	* Template Name: Search Template
	* Description: Template used for the search
	*/
?>

<?php get_header(); ?>

		<section>
			<div class="row">
				<div class="columns small-12">
					<form class="form-search">
						<div class="row">
							<div class="columns small-12">
								<label>Search near</label>
								
								<div style="position: relative">
									<input type="text" id="location-field" class="field-location" placeholder="Your location, address, region, city, postal code">
									
									<a class="btn-location"><i class="fa fa-map-o"></i></a>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="columns small-12">
								<label>Practitioner types:</label>
								
								<select id="practitioner-type" multiple="mulitple">
									<option>practitioner-type-1</option>
									<option>practitioner-type-2</option>
									<option>practitioner-type-3</option>
									<option>practitioner-type-4</option>
									<option>practitioner-type-5</option>
									<option>practitioner-type-6</option>
									<option>practitioner-type-7</option>
									<option>practitioner-type-8</option>
									<option>practitioner-type-9</option>
									<option>practitioner-type-10</option>
								</select>
							</div>
						</div>
						
						<br>
						
						<div class="row">
							<div class="columns small-12">
								<label>Practitioner condition:</label>
								
								<ul class="gfield_checkbox search-page" id="input_1_21">
							    	<li class="gchoice_1_21_1" style="display: list-item;">
        														<input name="input_21.1" type="checkbox" value="Orthotics" id="choice_1_21_1" tabindex="11">
        														<label for="choice_1_21_1" id="label_1_21_1">Orthotics</label>
        													</li>

        													<li class="gchoice_1_21_2" style="display: list-item;">
        														<input name="input_21.2" type="checkbox" value="IMS" id="choice_1_21_2" tabindex="12">
        														<label for="choice_1_21_2" id="label_1_21_2">IMS</label>
        													</li>

        													<li class="gchoice_1_21_3">
        														<input name="input_21.3" type="checkbox" value="Home Birth" id="choice_1_21_3" tabindex="13">
        														<label for="choice_1_21_3" id="label_1_21_3">Home Birth</label>
        													</li>

                                                            <li class="gchoice_1_21_4">
                                                            <input name="input_21.4" type="checkbox" value="Birth Center" id="choice_1_21_4" tabindex="14">
                                                            <label for="choice_1_21_4" id="label_1_21_4">Birth Center</label>
                                                            </li><li class="gchoice_1_21_5">
                                                            <input name="input_21.5" type="checkbox" value="Private Practice" id="choice_1_21_5" tabindex="15">
                                                            <label for="choice_1_21_5" id="label_1_21_5">Private Practice</label>
                                                            </li><li class="gchoice_1_21_6">
                                                            <input name="input_21.6" type="checkbox" value="Primary Care" id="choice_1_21_6" tabindex="16">
                                                            <label for="choice_1_21_6" id="label_1_21_6">Primary Care</label>
                                                            </li><li class="gchoice_1_21_7">
                                                            <input name="input_21.7" type="checkbox" value="Gynocologic Care" id="choice_1_21_7" tabindex="17">
                                                            <label for="choice_1_21_7" id="label_1_21_7">Gynocologic Care</label>
                                                            </li><li class="gchoice_1_21_8">
                                                            <input name="input_21.8" type="checkbox" value="Family Planning Services" id="choice_1_21_8" tabindex="18">
                                                            <label for="choice_1_21_8" id="label_1_21_8">Family Planning Services</label>
                                                            </li><li class="gchoice_1_21_9">
                                                            <input name="input_21.9" type="checkbox" value="Pregnancy Care" id="choice_1_21_9" tabindex="19">
                                                            <label for="choice_1_21_9" id="label_1_21_9">Pregnancy Care</label>
                                                            </li><li class="gchoice_1_21_11">
                                                            <input name="input_21.11" type="checkbox" value="Obstetrician Care" id="choice_1_21_11" tabindex="20">
                                                            <label for="choice_1_21_11" id="label_1_21_11">Obstetrician Care</label>
                                                            </li><li class="gchoice_1_21_12">
                                                            <input name="input_21.12" type="checkbox" value="Post Pardom Care" id="choice_1_21_12" tabindex="21">
                                                            <label for="choice_1_21_12" id="label_1_21_12">Post Pardom Care</label>
                                                            </li><li class="gchoice_1_21_13">
                                                            <input name="input_21.13" type="checkbox" value="Prescriptive Services" id="choice_1_21_13" tabindex="22">
                                                            <label for="choice_1_21_13" id="label_1_21_13">Prescriptive Services</label>
                                                            </li><li class="gchoice_1_21_14">
                                                            <input name="input_21.14" type="checkbox" value="Association Member" id="choice_1_21_14" tabindex="23">
                                                            <label for="choice_1_21_14" id="label_1_21_14">Association Member</label>
                                                            </li>
						    	</ul>
							</div>
						</div>
						
						<div class="row">
							<div class="columns small-12 text-center">
								<button class="button purple">Search</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>

<?php get_footer(); ?>