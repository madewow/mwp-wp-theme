<?php
	/**
	* Template Name: Register Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section>
			<header class="header-small" data-interchange="[img/header-home.jpg, small]">
				
			</header>
		</section>
		
		<section id="membership-level">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h3 class="text-center">YOU HAVE SELECTED<br><strong>PLATINUM</strong><br>MEMBERSHIP LEVEL</h3>
						
						<h4 class="text-center">The Price for Membership is <strong>$49.99/<sub>Month</sub></strong></h4>
					</div>
				</div>
			</header>
		</section>
		
		
		<form id="register-form">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Account Information</span>
						</h2>
						
						<p class="text-center"><em>Already have an account? Log in here</em></p>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="Username">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="email" placeholder="Email">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="email" placeholder="Confirm Email">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="password" placeholder="Password">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="password" placeholder="Confirm Password">
				</div>
			</div>
			
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Site Information</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="Site Name">
					
					<p class="text-center">Your address will be <strong>{Site Name}.mywellnesspro.com</strong>.<br>Site Name must be at least 4 characters, letters and numbers only. It cannot be changed so please choose carefully.</p>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1">
					<input type="text" placeholder="Site Title">
				</div>
			</div>
			
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Billing Address</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="First Name">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="Last Name">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="Address 1">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1">
					<input type="text" placeholder="Address 2">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="City">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="State">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="Postal Code">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<select>
						<option value="United States">Canada</option>
					</select>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1">
					<input type="text" placeholder="Phone">
				</div>
			</div>
			
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Billing Information</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="Credit Card Number">
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<div class="row">
						<div class="columns small-12 medium-6">
							<select>
								<option value="United States">Expire (month)</option>
							</select>
						</div>
						
						<div class="columns small-12 medium-6">
							<select>
								<option value="United States">Expire (year)</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<input type="text" placeholder="CVV">
					<p style="margin-top: -1rem; font-size: 0.75em"><a>What is this?</a></p>
				</div>
			</div>
			
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Membership Billing</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 required">
					<textarea></textarea>
					
					<div class="faux-checkbox">
						<input type="checkbox" id="agree-membership-billing">
						<label for="agree-membership-billing">I agree to the Membership Billing</label>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1 text-center">
					<br>
					
					<hr>
					
					<button class="button">SUBMIT &amp; Checkout</button>
				</div>
			</div>
		</form>
		
<?php get_footer(); ?>