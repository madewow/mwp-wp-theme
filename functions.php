<?php
/**
 * functions.php
 *
 */
 
function theme_styles() { 
	wp_enqueue_style( 'foundation-css', get_stylesheet_directory_uri() . '/css/foundation.css' );
	
	wp_enqueue_style( 'font-awesome-css', get_stylesheet_directory_uri() . '/css/font-awesome.min.css' );
	
	wp_enqueue_style( 'select2-css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' );
	
	wp_enqueue_style( 'app-css', get_stylesheet_directory_uri() . '/css/app.css' );
}
add_action('wp_enqueue_scripts', 'theme_styles');

function theme_scripts() {
	wp_register_script( 'google-map-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJIH7Ywy7JzOcsm9NhbpiRnCraSx5Dzk', array('jquery'), '1', true );
	
	// wp_register_script( 'jquery-js', get_stylesheet_directory_uri() . '/js/vendor/jquery.js', array('jquery'), '1', true );
	
	wp_register_script( 'what-input-js', get_stylesheet_directory_uri() . '/js/vendor/what-input.js', array('jquery'), '1', true );
	
	wp_register_script( 'foundation-js', get_stylesheet_directory_uri() . '/js/vendor/foundation.js', array('jquery'), '1', true );
	
	wp_register_script( 'select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js', array('jquery'), '1', true );
	
	wp_register_script( 'app-js', get_stylesheet_directory_uri() . '/js/app.js', array('jquery'), '1', true );
	
	wp_enqueue_script('google-map-js');
	// wp_enqueue_script('jquery-js');
	wp_enqueue_script('what-input-js');
	wp_enqueue_script('foundation-js');
	wp_enqueue_script('select2-js');
	wp_enqueue_script('app-js');
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' ); 


function register_my_menus() {
	register_nav_menus(
		array(
			'directory-links' => __( 'Directory Links', 'jointswp' ),
			'business-links' => __( 'Business Links', 'jointswp' ) // Secondary nav in footer
		)
	);
}
add_action( 'init', 'register_my_menus' );

// The Footer Menu
function footer_links($location) {
    wp_nav_menu(array(
    	'container' => 'false',                         // Remove nav container
    	'menu' => __( 'Footer Links', 'jointswp' ),   	// Nav name
    	'menu_class' => 'navigation-footer',      					// Adding custom nav class
    	'theme_location' => $location,             // Where it's located in the theme
        'depth' => 0,                                   // Limit the depth of the nav
    	'fallback_cb' => ''  							// Fallback function
	));
}


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );

add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
add_shortcode('caption', 'fixed_img_caption_shortcode');
function fixed_img_caption_shortcode($attr, $content = null) {
    if ( ! isset( $attr['caption'] ) ) {
        if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
        $content = $matches[1];
        $attr['caption'] = trim( $matches[2] );
        }
    }

    $output = apply_filters('img_caption_shortcode', '', $attr, $content);
    if ( $output != '' )
    return $output;

    extract(shortcode_atts(array(
        'id' => '',
        'align' => 'alignnone',
        'width' => '',
        'caption' => ''
    ), $attr));

    if ( 1 > (int) $width || empty($caption) )
    return $content;

    if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

    return '<figure ' . $id . '>' . do_shortcode( $content ) . '<figcaption>' . $caption . '</figcaptio></figure>';
}