<?php
	/**
	* Template Name: Practitioner Edit-1 Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section id="practitioner-header">
			<header class="header-small" data-interchange="[<?php bloginfo('stylesheet_directory'); ?>/img/header-home.jpg, small]">
				<div class="row">
					<div class="small-12 small-offseet-0 medium-10 medium-offset-1 text-center">
						<a class="camera" data-open="upload-background-modal">
							<i class="fa fa-camera"></i>
						</a>
					</div>
				</div>
			</header>
		</section>
		
		<section id="practitioner-info">
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1 text-center">
					<div class="thumb">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
						
						<a class="camera" data-open="upload-background-modal">
							<i class="fa fa-camera"></i>
						</a>
					</div>
					
					<h2 class="text-center">Jane Doe</h2>
					
					<p>Practitioner Type</p>
				</div>
			</div>
		</section>
		
		<form id="register-form">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Personal Information</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12">
							<label>Name</label>
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12 medium-6">
							<input type="text" placeholder="First Name">
						</div>
						
						<div class="columns small-12 medium-6">
							<input type="text" placeholder="Last Name">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<label>Email</label>
							<input type="email" placeholder="Email">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<label>Website</label>
							<input type="text" placeholder="http://">
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<label>Profile</label>
							
							<textarea></textarea>
						</div>
					</div>
				</div>
			</div>
			
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Practitioner Information</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-6">
							<select>
								<option>Practitioner Type</option>
							</select>
						</div>
						
						<div class="columns small-12 medium-6">
							<select>
								<option>Language</option>
							</select>
							
							<p><em>Select all that applies</em></p>
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<select>
								<option>Services</option>
							</select>
							
							<p><em>Select all that applies</em></p>
						</div>
					</div>
					
					<div class="row">
						<div class="columns small-12">
							<select>
								<option>Focus</option>
							</select>
							
							<p><em>Select all that applies</em></p>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1 text-right">
					<button class="cta-arrow">
						<i class="fa fa-arrow-right fa-lg"></i>
					</button>
				</div>
			</div>
		</form>
		
		<div class="reveal" id="upload-background-modal" data-reveal>
			<div class="row">
				<div class="columns small-12 text-center">
					<p>Upload Background Image</p>
					
					<h4>Drop file here to upload</h4>
					
					<p>or</p>
					
					<div class="container">
						<button class="button">Choose File</button>
						
						<input type="file">
					</div>
				</div>
			</div>
		</div>

<?php get_footer(); ?>